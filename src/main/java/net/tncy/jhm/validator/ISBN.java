package net.tncy.jhm.validator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({METHOD,FIELD,ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = ISBNValidator.class)
@Documented
public @interface ISBN {
    String message() default "{net.tncy.validator.constraints.books.ISBN}"; // Retourner le message d'erreur en cas de violation de test de validation
    Class<?>[] groups() default {}; // ce qui permet de retourner les groupes de validation quand on veut contextualiser : en fonction dede l'use case , on a une validation plus ou moins précise
    Class<? extends Payload>[] payload() default {}; //la valeur qu'on veut valider
    //payload fournit des données complémentaires généralement utilisés lors de l'exploitation des violations de contraintes
    //groups : définir le ou les groupes de validation auxquels la contrainte appartient. La valeur par défaut doit être un tableau vide de type Class<?>
}
